package nowcode_alg_oj

import org.scalatest.FunSuite

/*
https://www.nowcoder.com/ta/job-code-high
 */
class JobCodeHighSuite extends FunSuite {
  test("NC78 反转链表") {
    /*
    输入一个长度为n链表，反转链表后，输出新链表的表头。

数据范围： n\leq1000n≤1000
要求：空间复杂度 O(1)O(1) ，时间复杂度 O(n)O(n) 。

如当输入链表{1,2,3}时，
经反转后，原链表变为{3,2,1}，所以对应的输出为{3,2,1}。

示例1:
输入：
{1,2,3}
复制
返回值：
{3,2,1}

示例2:
输入：
{}
复制
返回值：
{}
复制
说明：
空链表则输出空
     */
    class ListNode(var value: Int) {
       var next: ListNode = null
    }

    object Solution {
      /**
       * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
       *
       * @param head ListNode类
       * @return ListNode类
       */
      def ReverseList(head: ListNode): ListNode = {
        // write code here
        if(head != null)
          head.next
        else
          head

      }
    }



  }

}
