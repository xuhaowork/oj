# 描述
# 一个机器人在m×n大小的地图的左上角（起点）。
# 机器人每次可以向下或向右移动。机器人要到达地图的右下角（终点）。
# 可以有多少种不同的路径从起点走到终点？
#
# 备注：m和n小于等于100,并保证计算结果在int范围内
#
# 数据范围：0 < n,m \le 1000<n,m≤100，保证计算结果在32位整型范围内
# 要求：空间复杂度 O(nm)O(nm)，时间复杂度 O(nm)O(nm)
# 进阶：空间复杂度 O(1)O(1)，时间复杂度 O(min(n,m))O(min(n,m))
# 示例1
# 输入：
# 2,1
# 复制
# 返回值：
# 1
# 复制
# 示例2
# 输入：
# 2,2
# 复制
# 返回值：
# 2
#
# 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
#
#
# @param m int整型
# @param n int整型
# @return int整型
#
class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        d = [[0 for i in range(0, n)] for _ in range(0, m)]
        for m_ in range(m):
            for n_ in range(n):
                if m_ == 0 or n_ == 0:
                    d[m_][n_] = 1
                else:
                    d[m_][n_] = d[m_][n_ - 1] + d[m_ - 1][n_]
        return d[m-1][n-1]


# C_{m+n}_n
#
# d[m][n]到第m+1行n+1列的路径条数
# 初始化d[0][:] = 1, d[:][0] = 1
# d[m][n] = d[m][n-1]+d[m-1][n]
solution = Solution()
print(solution.uniquePaths(1, 2))
