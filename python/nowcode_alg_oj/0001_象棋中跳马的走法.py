# 链接：https://www.nowcoder.com/questionTerminal/c45704a41617402fb5c34a1778bb2645
# 来源：牛客网
#
# [编程题]象棋中马的跳法
# 热度指数：241时间限制：C/C++ 1秒，其他语言2秒空间限制：C/C++ 256M，其他语言512M
# 算法知识视频讲解
# 请同学们自行搜索或者想象一个象棋的棋盘，然后把整个棋盘放入第一象限，棋盘的最左下角是(0,0)位置。
# 那么整个棋盘就是横坐标上9条线、纵坐标上10条线的一个区域。给你三个参数，x，y，k，
# 返回如果“马”从(0,0)位置出发，必须走k步，最后落在(x,y)上的方法数有多少种？
#
# 输入描述:
# 输入一行数字，包含3个参数x，y，k
#
#
# 输出描述:
# 输出一个数，代表有几种
# 示例1
# 输入
# 7 7 10
# 输出
# 515813
#
# 动态规划
# - d[x, y, k]表示从0, 0用k步跳到x, y的路径数
# - d[0, 0, 0] = 0
# - d[x, y, k] = d[x+-2, y+-1, k-1] + d[x+-1, y+-2, k-1]
#
from io import StringIO
import sys

# line = StringIO('''7 7 10''')
line = sys.stdin
_x, _y, _k = map(int, line.readline().strip().split(' '))

d = [{} for _ in range(_k + 3)]
d[0][(0, 0)] = 1

for k in range(1, _k + 1):
    for x in range(0, 9):
        for y in range(0, 10):
            for x_i, y_i in [(x + 2, y + 1), (x - 2, y + 1), (x + 2, y - 1), (x - 2, y - 1), (x + 1, y + 2),
                             (x - 1, y + 2),
                             (x + 1, y - 2), (x - 1, y - 2)]:
                if (x_i, y_i) in d[k - 1]:
                    if (x, y) in d[k]:
                        d[k][(x, y)] += d[k-1][(x_i, y_i)]
                    else:
                        d[k][(x, y)] = d[k-1][(x_i, y_i)]

if (_x, _y) in d[_k]:
    print(d[_k][(_x, _y)])
else:
    print(0)
