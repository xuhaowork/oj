from typing import Optional, Tuple


def find_mini_divisor(string: str) -> Optional[Tuple[int, str]]:
    """
    找到重复的字串, n*字串=string, 该字串是最小的
    :param string:
    :return:
    """
    loc_ = (string * 2).find(string, 1)
    return len(string) // loc_, string[:loc_]


assert find_mini_divisor('abcabc') == (2, 'abc')
assert find_mini_divisor('aaaaa') == (5, 'a')
assert find_mini_divisor('abcd') == (1, 'abcd')
