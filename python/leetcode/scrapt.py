from sklearn.datasets import load_iris

# 读取鸢尾花数据集
from sklearn.neighbors import KNeighborsTransformer

iris = load_iris()
x = iris.data
y = iris.target
k_range = range(1, 31)
k_error = []

knn = KNeighborsTransformer(n_neighbors=3)
knn.fit(x)
_, nbs = knn.kneighbors(x)
print(nbs)
