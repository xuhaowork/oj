# 43. 字符串相乘
# 给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。
#
# 示例 1:
#
# 输入: num1 = "2", num2 = "3"
# 输出: "6"
# 示例 2:
#
# 输入: num1 = "123", num2 = "456"
# 输出: "56088"
# 说明：
#
# num1 和 num2 的长度小于110。
# num1 和 num2 只包含数字 0-9。
# num1 和 num2 均不以零开头，除非是数字 0 本身。
# 不能使用任何标准库的大数类型（比如 BigInteger）或直接将输入转换为整数来处理。
class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        num_array1 = [int(each) for each in num1]
        num_array2 = [int(each) for each in num2]
        res_array = [0 for _ in range(len(num1) + len(num2))]
        for i in range(len(num_array2)):
            for j in range(len(num_array1)):
                res_array[i + j] += num_array2[-i - 1] * num_array1[-j - 1]
        res = ''
        rest = 0
        for each in res_array:
            res = str((each + rest) % 10) + res
            rest = (each + rest) // 10
        not_met_non_zero = True
        ff = ''
        for each in res:
            if not_met_non_zero and int(each):
                not_met_non_zero = False
            if not not_met_non_zero:
                ff += each
        return ff if len(ff) else '0'


solution = Solution()
assert solution.multiply('0', '2') == '0'
assert solution.multiply('123', '456') == '56088'


