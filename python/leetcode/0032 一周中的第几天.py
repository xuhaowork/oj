# https://leetcode-cn.com/problems/day-of-the-week/
# 1185. 一周中的第几天
# 给你一个日期，请你设计一个算法来判断它是对应一周中的哪一天。
#
# 输入为三个整数：day、month 和 year，分别表示日、月、年。
#
# 您返回的结果必须是这几个值中的一个 {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}。
#
#
#
# 示例 1：
#
# 输入：day = 31, month = 8, year = 2019
# 输出："Saturday"
# 示例 2：
#
# 输入：day = 18, month = 7, year = 1999
# 输出："Sunday"
# 示例 3：
#
# 输入：day = 15, month = 8, year = 1993
# 输出："Sunday"
#
#
# 提示：
#
# 给出的日期一定是在 1971 到 2100 年之间的有效日期。
class Solution:
    def dayOfTheWeek(self, day: int, month: int, year: int) -> str:
        week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        # 距离1970年12月31日 周四的日期数
        days = day
        days += (year - 1971) * 365
        # 四年一闰, 百年不闰, 四百年一闰月, 刚好只出现了一次百年2100年
        days += (year - 1969) // 4
        days_of_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        days += sum(days_of_month[:month])
        # 是否经过闰年中的二月
        if month > 2 and ((year % 4 == 0 and year % 100) or year % 400 == 0):
            days += 1
        return week[(days + 3) % 7]


solution = Solution()
assert solution.dayOfTheWeek(1, 1, 1971) == 'Friday'
assert solution.dayOfTheWeek(18, 7, 1999) == 'Sunday'
assert solution.dayOfTheWeek(15, 8, 1993) == 'Sunday'
