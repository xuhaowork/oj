# 198. 打家劫舍
# 你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
#
# 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。
#
#
#
# 示例 1：
#
# 输入：[1,2,3,1]
# 输出：4
# 解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
#      偷窃到的最高金额 = 1 + 3 = 4 。
# 示例 2：
#
# 输入：[2,7,9,3,1]
# 输出：12
# 解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
#      偷窃到的最高金额 = 2 + 9 + 1 = 12 。
#
#
# 提示：
#
# 1 <= nums.length <= 100
# 0 <= nums[i] <= 400
#
# 题解:
# - d[i]为前i间屋的最大价值, 最后两间屋肯定有一间要用到
# - 初始化: d[0] = 0, d[1] = nums[0]
# - if d[i] == d[i-1]: d[i-1]+num[i]
# - if d[i] > d[i-1]: max(d[i-1]+num[i], d[i])
from typing import List


class Solution:
    def rob(self, nums: List[int]) -> int:
        d = [0 for _ in range(len(nums) + 1)]
        if not len(nums):
            return 0
        d[1] = nums[0]
        for i in range(1, len(nums)):
            if d[i] == d[i - 1]:
                d[i + 1] = d[i - 1] + nums[i]
            else:
                d[i + 1] = max(d[i - 1] + nums[i], d[i])
        return d[len(nums)]


solution = Solution()
assert solution.rob([1, 2, 3, 1]) == 4
assert solution.rob([2, 7, 9, 3, 1]) == 12
