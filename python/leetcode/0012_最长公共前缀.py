# 14. 最长公共前缀
# 编写一个函数来查找字符串数组中的最长公共前缀。
#
# 如果不存在公共前缀，返回空字符串 ""。
#
#
#
# 示例 1：
#
# 输入：strs = ["flower","flow","flight"]
# 输出："fl"
# 示例 2：
#
# 输入：strs = ["dog","racecar","car"]
# 输出：""
# 解释：输入不存在公共前缀。
#
#
# 提示：
#
# 1 <= strs.length <= 200
# 0 <= strs[i].length <= 200
# strs[i] 仅由小写英文字母组成
# ----
# 讨论:
# python编程时'if x:'时
# 不要混淆了None和0
from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if not len(strs):
            return ''
        min_len = min(map(len, strs))
        res = None
        for i in range(min_len):
            char1 = strs[0][i]
            if not all(map(lambda char: char[i] == char1, strs)):
                break
            res = i

        return strs[0][:(res + 1)] if res is not None else ''


solution = Solution()
# assert solution.longestCommonPrefix(["flower", "flow", "flight"]) == "fl"
# assert solution.longestCommonPrefix(["dog", "racecar", "car"]) == ""
assert solution.longestCommonPrefix(["a"]) == "a"
