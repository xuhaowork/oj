# 12. 整数转罗马数字
# 罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
#
# 字符          数值
# I             1
# V             5
# X             10
# L             50
# C             100
# D             500
# M             1000
# 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。
#
# 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：
#
# I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
# X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
# C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
# 给你一个整数，将其转为罗马数字。
#
#
#
# 示例 1:
#
# 输入: num = 3
# 输出: "III"
# 示例 2:
#
# 输入: num = 4
# 输出: "IV"
# 示例 3:
#
# 输入: num = 9
# 输出: "IX"
# 示例 4:
#
# 输入: num = 58
# 输出: "LVIII"
# 解释: L = 50, V = 5, III = 3.
# 示例 5:
#
# 输入: num = 1994
# 输出: "MCMXCIV"
# 解释: M = 1000, CM = 900, XC = 90, IV = 4.
#
#
# 提示：
#
# 1 <= num <= 3999

class Solution:
    def intToRoman(self, num: int) -> str:
        res = ''
        # 千位
        thousand = num // 1000
        if 1 <= thousand <= 3:
            res += thousand * 'M'
        num = num % 1000
        # 百位
        hundred = num // 100
        if 1 <= hundred <= 3:
            res += hundred * 'C'
        elif hundred == 4:
            res += 'CD'
        elif 5 <= hundred < 9:
            res += 'D'
            res += (hundred - 5) * 'C'
        elif hundred == 9:
            res += 'CM'
        # 十位
        num = num % 100
        tens = num // 10
        if 1 <= tens <= 3:
            res += tens * 'X'
        elif tens == 4:
            res += 'XL'
        elif 5 <= tens < 9:
            res += 'L'
            res += (tens - 5) * 'X'
        elif tens == 9:
            res += 'XC'
        # 个位
        num = num % 10
        if 1 <= num <= 3:
            res += num * 'I'
        elif num == 4:
            res += 'IV'
        elif 5 <= num < 9:
            res += 'V'
            res += (num - 5) * 'I'
        elif num == 9:
            res += 'IX'
        return res


solution = Solution()
assert solution.intToRoman(3) == "III"
assert solution.intToRoman(4) == "IV"
assert solution.intToRoman(9) == "IX"
assert solution.intToRoman(58) == "LVIII"
assert solution.intToRoman(1994) == "MCMXCIV"

